var tf = require("@tensorflow/tfjs");
var lmlMobilenet = require("@tensorflow-models/mobilenet");
var knnClassifier = require("@tensorflow-models/knn-classifier");
var BrainText = require("brain-text");


function FeatureExtractorText(labeledDataManager) {
    this.labeledDataManager = labeledDataManager;
    this.braintext = new BrainText('es');
}

/**
 * 
 * @param {Array(Object)} traindata 
 */
FeatureExtractorText.prototype.buildClasses = function (traindata) {
    this.classes = {};
    // extract all the classe (which are the labels) without
    // repetition from traindata and map to number in order to
    // feed the ANN
    let i = 0;
    for (let data of traindata) {
        if (this.classes[data.label] == undefined) {
            this.classes[data.label] = i;
            i++;
        }
    }
}

/**
 * 
 * @returns {Array(Object)}
 */
FeatureExtractorText.prototype.getRawDataset = function () {
    let rawDataset = [];
    for (let label of this.labeledDataManager.labelsWithData.keys()) {
        for (let data of this.labeledDataManager.labelsWithData.get(label)) {
            let e = { label: label, text: data }
            rawDataset.push(e)
        }
    }
    return rawDataset;
}

/**
 * 
 * @param {number} res 
 * @param {number} num_classes 
 * @returns 
 */
FeatureExtractorText.prototype.labelOneShotEncoding = function (res, num_classes) {
    let vec = [];
    for (let i = 0; i < num_classes; i += 1) {
        vec.push(0);
    }
    vec[res] = 1;
    return vec;
}

FeatureExtractorText.prototype.updateInfrastructure = function () {
    let rawDataset = this.getRawDataset();
    this.buildClasses(rawDataset)
    this.braintext.setTraindata(rawDataset);
    this.braintext.updateInfrastructure();

    return rawDataset;
}

/**
 * 
 * @param {string} text 
 * @returns Promise
 */
FeatureExtractorText.prototype.extractInstanceFeatures = function (text) {

    let feature = this.braintext.extractFeature(text);
    let tensor_inputs = tf.cast(tf.stack(feature), 'float32');

    return new Promise((resolve, reject) => {
        let shape = tensor_inputs.shape;
        resolve(tensor_inputs.reshape([1, shape[0]]));
    })
}

/**
 * 
 * @returns Promise<{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }>
 */
FeatureExtractorText.prototype.extractDatasetFeatures = async function () {
    // first the map structure of data stored in labeledDataManager 
    // is reorganized as a list of pairs {label, data}

    let rawDataset = this.updateInfrastructure();

    let traindata_for_ann = this.braintext.prepareTrainData();

    let inputs = [];
    let labels = [];


    for (let data of rawDataset) {
        inputs.push(data.text);
        labels.push(this.labelOneShotEncoding(
            this.classes[data.label], Object.keys(this.classes).length))
    }

    let text_labels = Object.keys(this.classes)
    let tensor_labels = tf.cast(tf.stack(labels), 'int32');
    let tensor_inputs = tf.cast(tf.stack(traindata_for_ann), 'float32');

    return await new Promise((resolve, reject) => {
        resolve({ text_labels, tensor_labels, tensor_inputs });
    })
}


function FeatureExtractorNumbers(labeledDataManager) {
    this.labeledDataManager = labeledDataManager;
}

FeatureExtractorNumbers.prototype.buildClasses = function (traindata) {
    this.classes = {};
    // extract all the classe (which are the labels) without
    // repetition from traindata and map to number in order to
    // feed the ANN
    let i = 0;
    for (let data of traindata) {
        if (this.classes[data.label] == undefined) {
            this.classes[data.label] = i;
            i++;
        }
    }
}

FeatureExtractorNumbers.prototype.getRawDataset = function () {
    let rawDataset = [];
    for (let label of this.labeledDataManager.labelsWithData.keys()) {
        for (let data of this.labeledDataManager.labelsWithData.get(label)) {
            let e = { label: label, numbers: data }
            rawDataset.push(e)
        }
    }
    return rawDataset;
}

/**
 * 
 * @param {*} rawDataset 
 * @returns 
 */
FeatureExtractorNumbers.prototype.prepareTrainData = function (rawDataset) {
    let traindata_for_ann = [];
    for (let data of rawDataset) {
        traindata_for_ann.push(data["numbers"].split(",").map(v => parseFloat(v)));
    }
    return traindata_for_ann;
}

/**
 * 
 * @param {number} res 
 * @param {number} num_classes 
 * @returns 
 */
FeatureExtractorNumbers.prototype.labelOneShotEncoding = function (res, num_classes) {
    let vec = [];
    for (let i = 0; i < num_classes; i += 1) {
        vec.push(0);
    }
    vec[res] = 1;
    return vec;
}

/**
 * 
 * @param {string} text 
 * @returns Promise<tf.Tensor>
 */
FeatureExtractorNumbers.prototype.extractInstanceFeatures = function (text) {
    let feature = text.split(",").map(v => parseFloat(v));
    let tensor_inputs = tf.cast(tf.stack(feature), 'float32');
    return new Promise((resolve, reject) => {
        let shape = tensor_inputs.shape;
        resolve(tensor_inputs.reshape([1, shape[0]]));
    })
}

/**
 * 
 * @returns Promise < { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor } >
 */
FeatureExtractorNumbers.prototype.extractDatasetFeatures = function () {

    let rawDataset = this.getRawDataset();
    this.buildClasses(rawDataset);

    let traindata_for_ann = this.prepareTrainData(rawDataset);

    let labels = [];

    for (let data of rawDataset) {
        labels.push(this.labelOneShotEncoding(
            this.classes[data.label], Object.keys(this.classes).length))
    }

    let text_labels = Array.from(this.labeledDataManager.labelsWithData.keys());
    let tensor_labels = tf.cast(tf.stack(labels), 'int32');
    let tensor_inputs = tf.cast(tf.stack(traindata_for_ann), 'float32');

    return new Promise((resolve, reject) => {
        resolve({ text_labels, tensor_labels, tensor_inputs });
    })
}

const IMAGE_SIZE = 227;

function FeatureExtractorImage(labeledDataManager) {
    this.labeledDataManager = labeledDataManager;
    this.labels = [];
    this.dataset = {
        data: [],
        labels: [],
        dataArray: []
    };
}

FeatureExtractorImage.prototype.loadMobilenet = function () {
    return lmlMobilenet.load();
}

FeatureExtractorImage.prototype.addLabeledImage = function (labeledImage) {

    // si la label (string) no existe en el array de labels, la añadimos
    if (this.labels.indexOf(labeledImage.label) == -1) {
        this.labels.push(labeledImage.label);
    }

    return this.extractInstanceFeatures(labeledImage.data).then((t_activation) => {
        let t = t_activation.squeeze();
        this.dataset.data.push(t);
        this.dataset.dataArray.push(t.dataSync());
        // Añadimos al array de labels del dataset el índice que el corresponde
        // a la label en el array labels.
        this.dataset.labels.push(this.labels.indexOf(labeledImage.label));

        t_activation.dispose();
    })

}

FeatureExtractorImage.prototype.buildDataset = function () {
    this.labels = [];
    this.dataset.labels = [];
    for (let t of this.dataset.data) {
        t.dispose();
    }
    this.dataset.data = [];
    this.dataset.dataArray = [];
    let promises = [];
    for (const k of this.labeledDataManager.labelsWithData.keys()) {
        for (const src of this.labeledDataManager.labelsWithData.get(k)) {
            let p = new Promise(resolve => {
                let image = new Image();
                image.src = src;
                image.onload = () => {
                    this.addLabeledImage({ label: k, data: image }).then(() => {
                        resolve(true);
                    })
                }
            });
            promises.push(p);
        }
    }

    return Promise.all(promises);
}

/**
 * 
 * @param {HTMLImageElement} image 
 * @returns Promise<tf.Tensor>
 */
FeatureExtractorImage.prototype.extractInstanceFeatures = function (image) {
    image.width = IMAGE_SIZE;
    image.height = IMAGE_SIZE;
    if ( typeof SNAP != "undefined") {
    /**
     * Lo de meter la imagen en un canvas y luego convertirla en  un ImageData
     * se ha hecho porque en Snap! el método `tf.browser.fromPixels()` no
     * funciona bien con la imagen que se construye a partir de la codificación
     * base64 (src) que se ha llevado a cabo anteriormente en otros métodos.
     * No sé la razón de este fallo, lo he investigado a fondo pero no lo he 
     * llegado a entender. Sin embargo, en dicha investigación comprobé que
     * un ImageData como argumento de `tf.browser.fromPixels()` funciona mejor.
     * Se puede estar tentado a usar directamente el canvas que proporciona Snap!
     * pero eso haría que la librería no fuese compatible con Scratch, así que no
     * me queda más remedio que hacer esta "forzada" conversión. 
    */
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0);
        image = ctx.getImageData(0, 0, IMAGE_SIZE, IMAGE_SIZE);
    }

    const t_image = tf.browser.fromPixels(image);
    return this.loadMobilenet().then(m => {
        this.mobilenet = m;
        const t_activation = this.mobilenet.infer(t_image, 'conv_preds');
        t_image.dispose();
        return t_activation;
    })

}

/**
 * This is the method which must be called to convert the raw data stored in
 * LabeledDataManagerService to an object containing:
 * 
 *  - the `text_labels`
 *        is an array of strings wich elements are the text labels
 *  - the `tensor_labels`
 *        is a tf.Tensor object with shape  [num_examples, num_labels], where:
 *            - num_examples, is the number of examples in the dataset and
 *            - num_labels, is the number of classes(labels) to be recognized.
 *       The elements of this tensor are Int32 data ranging from 0 to 
 *        num_labels - 1, and represent the index of the corresponding text
 *        label in `text_labels`
 *  - the `tensor_inputs`
 *        is a tf.Tensor object with shape [num_example, 1024] where:
 *            - num_examples, is the number of examples in the dataset.
 *        The images are converted in a 1024 vector when passed through the
 *        `extractFeature()` method. This explain the second element of the 
 *        shape.
 *        The elements of this tensor are Arrays of 1024 float32 numbers, and 
 *        each Array represents an image of the dataset. 
 * 
 *  
 * @returns {text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor}
 */
FeatureExtractorImage.prototype.extractDatasetFeatures = function () {
    console.log("llalala");
    return this.buildDataset().then(() => {
        console.log("buildDataset then");
        const inputs = tf.stack(this.dataset.data);
        const labels = tf.tidy(() => tf.oneHot(tf.tensor1d(this.dataset.labels, 'int32'), this.labels.length));

        return { text_labels: this.labels, tensor_labels: labels, tensor_inputs: inputs };

    });
}

function FeatureExtractor(labeledDataManager) {
    let modelType = labeledDataManager.modelType;
    switch (modelType) {
        case "text":
            this.featureExtractor = new FeatureExtractorText(labeledDataManager);
            break;
        case "numerical":
            this.featureExtractor = new FeatureExtractorNumbers(labeledDataManager);
            break;
        case "image":
            this.featureExtractor = new FeatureExtractorImage(labeledDataManager);
            break;
    }
}

FeatureExtractor.prototype.extractInstanceFeatures = function (feature) {
    return this.featureExtractor.extractInstanceFeatures(feature);
}

FeatureExtractor.prototype.extractDatasetFeatures = function () {
    return this.featureExtractor.extractDatasetFeatures();
}
/**
 * 
 * @param {*} params 
 * 
 * Example params:
 * {
    "validationSplit": 0,
    "knn": {
        "k": 5
    },
    "neural_network": {
        "epochs": 20,
        "batchSize": 10,
        "learningRate": 0.001
    }
}
 */
function MlAlgorithmNeuralNetwork(params) {
    this.mlModel = null;
    this.params = params;
}

/**
 * 
 * @param {number} inputShape 
 * @param {number} outputShape 
 */
MlAlgorithmNeuralNetwork.prototype.buildModel = function (inputShape, outputShape) {
    /**
    * Creamos un modelo consistente en una red neuronal con una capa de entrada,
    * una capa oculta y una capa de salida. 
    */
    if (this.mlModel != null) {
        this.mlModel.dispose();
        this.mlModel = null;
    }

    this.mlModel = tf.sequential({
        layers: [
            tf.layers.dense({
                units: 200,
                inputShape: [inputShape],
                activation: 'relu'
            }),
            tf.layers.dense({
                units: 100,
                activation: 'relu',
                kernelInitializer: 'varianceScaling',
                useBias: true
            }),
            tf.layers.dense({
                units: outputShape,
                kernelInitializer: 'varianceScaling',
                useBias: false,
                activation: 'softmax'
            })
        ]
    });

    const optimizer = tf.train.adam(this.params.learningRate)

    this.mlModel.compile({
        optimizer: optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy']
    });
}

/**
 * 
 * @param {{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }} features 
 * @param {[tf.Tensor, tf.Tensor]} validationData 
 * @returns Promise < tf.History >
 */
MlAlgorithmNeuralNetwork.prototype.train = function (features, validationData) {

    //this.labeledDataManager.state = MLState.TRAINING;
    const inputShape = features.tensor_inputs.shape[1];
    const labelsShape = features.tensor_labels.shape[1];
    this.buildModel(inputShape, labelsShape);
    let that = this;
    function onBatchEnd(batch, logs) {
        console.log('Accuracy', logs.acc);
    }

    //Train for 5 epochs with batch size of 32.
    return this.mlModel.fit(features.tensor_inputs, features.tensor_labels, {
        epochs: this.params.epochs,
        batchSize: this.params.batchSize,
        callbacks: { onBatchEnd },
        shuffle: true,
        //validationSplit: this.params.neural_network.validationSplit/100,
        validationData: validationData
    }).then(info => {
        //this.mlModel.save('indexeddb://lml-ann-model');
        return { mlModel: this.mlModel, info: info }
    });
}

/**
 * 
 * @param {tf.Tensor} input_tensor 
 * @param {string[]} labels 
 * @returns Promise<[string, number][]>
 */
MlAlgorithmNeuralNetwork.prototype.classify = function (input_tensor, labels) {
    const prediction = this.mlModel.predict(input_tensor);
    input_tensor.dispose();
    const predictions = prediction.dataSync();
    const arr_predictions = Array.from(predictions);
    let results = [];
    for (let i = 0; i < arr_predictions.length; i++) {
        results.push([labels[i], arr_predictions[i]]);
    }
    results.sort((a, b) => b[1] - a[1]);
    return new Promise((resolve, reject) => { resolve(results) });
}

function MlAlgorithmKNN(params) {
    this.mlModel = null;
    this.params = params
}

/**
 * 
 * @param {{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }} features 
 * @returns 
 */
MlAlgorithmKNN.prototype.buildModel = function (features) {
    this.mlModel = knnClassifier.create();
    let labels = tf.unstack(features.tensor_labels);
    let inputs = tf.unstack(features.tensor_inputs);

    for (let index in labels) {
        this.mlModel.addExample(inputs[index], labels[index]);
    }
    return true
}

/**
 * 
 * @param {{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }} features 
 * @returns Promise < any >
 */
MlAlgorithmKNN.prototype.train = function (features) {
    console.log("Training with KNN");
    this.mlModel = knnClassifier.create();

    /**
     * 
     * @param {tf.Tensor} label 
     * @returns 
     */
    let getClassIndex = (label) => {
        let l = label.arraySync();
        return l.indexOf(1);
    }

    let _labels = tf.unstack(features.tensor_labels);
    let labels = []
    for (let label of _labels) {
        labels.push(getClassIndex(label));
    }

    let inputs = tf.unstack(features.tensor_inputs);

    for (let index in labels) {
        console.log(`${index} -> ${inputs[index]}, ${labels[index]}`)
        this.mlModel.addExample(inputs[index], labels[index]);
    }

    let result = {
        mlModel: this.mlModel,
        info: true
    }
    return new Promise((resolve, reject) => { resolve(result) })

}

/**
 * 
 * @param {tf.Tensor} input_tensor 
 * @param {string[]} labels 
 * @returns Promise < [string, number][] >
 */
MlAlgorithmKNN.prototype.classify = function (input_tensor, labels) {
    let k = this.params.k;
    return this.mlModel.predictClass(input_tensor, k).then(prediction => {
        let results = [];
        for (let i in prediction.confidences) {
            results.push([labels[i], prediction.confidences[i]]);
        }
        results.sort((a, b) => b[1] - a[1]);
        input_tensor.dispose();
        return results;
    })
}

function MlAlgorithm(labeledDataManager) {
    this.features = null;
    if (labeledDataManager.mlAlgorithm == 'knn') {
        this.mlAlgorithm = new MlAlgorithmKNN(labeledDataManager.algorithms_params.knn);
    } else {
        this.mlAlgorithm = new MlAlgorithmNeuralNetwork(labeledDataManager.algorithms_params.neural_network);
    }
}

MlAlgorithm.prototype.train = function (features) {
    this.features = features;
    return this.mlAlgorithm.train(features);
}

MlAlgorithm.prototype.classify = function (input_tensor) {
    return this.mlAlgorithm.classify(input_tensor, this.features.text_labels)
        .then(c => {
            console.log(c);
            return c;
        });
}


MlAlgorithm.prototype.mostLikelyClassification = function (input_tensor) {
    return this.classify(input_tensor).then(prediction => {
        let sortedPrediction = prediction.sort(function (a, b) { return b[1] - a[1]; });
        return sortedPrediction[0][0];
    })
}

MlAlgorithm.prototype.mostLikelyConfidence = function (input_tensor) {
    return this.classify(input_tensor).then(prediction => {
        let sortedPrediction = prediction.sort(function (a, b) { return b[1] - a[1]; });
        return sortedPrediction[0][1];
    })
}

module.exports = {
    FeatureExtractorText,
    FeatureExtractorNumbers,
    FeatureExtractorImage,
    FeatureExtractor,
    MlAlgorithmNeuralNetwork,
    MlAlgorithmKNN,
    MlAlgorithm
};