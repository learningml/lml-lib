const { Tensor } = require('@tensorflow/tfjs-core');

describe("FeatureExtractorText", function () {
  const LML_Algorithms = require('../index');
  const BrainText = require("brain-text");
  const Utils = require("./utils");

  let fet;
  let labelsWithData = new Map();
  let utils = new Utils();
  utils.createMapLabelsWithData(labelsWithData);

  beforeEach(function () {
    fet = new LML_Algorithms.FeatureExtractorText(labelsWithData);
  });

  it("Create FeatureExtractorText", function () {
    expect(fet).toBeInstanceOf(LML_Algorithms.FeatureExtractorText);
    expect(fet.braintext).toBeInstanceOf(BrainText);
    expect(fet.labelsWithData).toBeInstanceOf(Map);
  });

  it("Extract Text Dataset Features types", function () {
    return fet.extractDatasetFeatures().then(function (features) {
      expect(features.text_labels).toBeInstanceOf(Array);
      expect(features.tensor_labels).toBeInstanceOf(Tensor);
      expect(features.tensor_inputs).toBeInstanceOf(Tensor);
      expect(features.tensor_labels.shape).toEqual([31, 4]);
      expect(features.tensor_inputs.shape).toEqual([31, 137]);
    });
  });

  it("Extract Text one feature", function () {
    return fet.extractInstanceFeatures("Esto es una prueba").then(function (feature) {
      expect(feature).toBeInstanceOf(Tensor);
    });
  });

});


describe("FeatureExtractorNumbers", function () {
  const LML_Algorithms = require('../index');
  const Utils = require("./utils");

  let fet;
  let labelsWithData = new Map();
  let utils = new Utils();
  utils.createMapLabelsWithDataNumber(labelsWithData);

  beforeEach(function () {
    fet = new LML_Algorithms.FeatureExtractorNumbers(labelsWithData);
  });

  it("Create FeatureExtractorNumbers", function () {
    expect(fet).toBeInstanceOf(LML_Algorithms.FeatureExtractorNumbers);
    expect(fet.labelsWithData).toBeInstanceOf(Map);
  });

  it("Extract Numbers Dataset Features types", function () {
    return fet.extractDatasetFeatures().then(function (features) {
      expect(features.text_labels).toBeInstanceOf(Array);
      expect(features.tensor_labels).toBeInstanceOf(Tensor);
      expect(features.tensor_inputs).toBeInstanceOf(Tensor);
      expect(features.tensor_labels.shape).toEqual([147, 3]);
      expect(features.tensor_inputs.shape).toEqual([147, 4]);
    });
  });

  it("Extract Numbers one feature", function () {
    return fet.extractInstanceFeatures("1,2,3").then(function (feature) {
      expect(feature).toBeInstanceOf(Tensor);
    });
  });

});

describe("FeatureExtractorImage", function () {
  const LML_Algorithms = require('../index');

  let fet;
  let labelsWithData = new Map();

  beforeEach(function () {
    fet = new LML_Algorithms.FeatureExtractorImage(labelsWithData);
  });

  it("Create FeatureExtractorImage", function () {
    expect(fet).toBeInstanceOf(LML_Algorithms.FeatureExtractorImage);
    expect(fet.labelsWithData).toBeInstanceOf(Map);
  });

})
