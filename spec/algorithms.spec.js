const { Tensor } = require('@tensorflow/tfjs-core');
var tf = require("@tensorflow/tfjs");

describe("MlAlgorithmNeuralNetwork", function () {
    const LML_Algorithms = require('../index');
    const Utils = require("./utils");

    let algorithm;
    let labelsWithData = new Map();
    let utils = new Utils();
    utils.createMapLabelsWithData(labelsWithData);
    let fet = new LML_Algorithms.FeatureExtractorText(labelsWithData);
    let params = {
        "validationSplit": 0,
        "knn": {
            "k": 5
        },
        "neural_network": {
            "epochs": 20,
            "batchSize": 10,
            "learningRate": 0.001
        }
    };

    beforeEach(function () {
        algorithm = new LML_Algorithms.MlAlgorithmNeuralNetwork(params);
    });

    it("MlAlgorithmNeuralNetwork train", function () {
        fet.extractDatasetFeatures().then(features => {
            expect(features).toBeDefined();
            algorithm.train(features).then(r => {
                console.log(r);
                expect(r).toBeInstanceOf(tf.History);
            });
        })
    });

    it("MlAlgorithmNeuralNetwork classify", function () {
        let labels;
        fet.extractDatasetFeatures().then(features => {
            labels = features.labels;
            return algorithm.train(features);
        }).then(r => {
            return fet.extractInstanceFeatures("Conjunto de montañas");
        }).then(input_tensor => {
            return algorithm.classify(input_tensor, labels);
        }).then(result => {
            console.log(result);
            expect(result).toBe(6);
        })
    });
})