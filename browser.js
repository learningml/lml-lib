const LMLAlgorithms = require("./lml_algorithms");
const LMLLabeledDataManager = require("./lml_labeled_data_manager");
const LMLMessageProtocol = require("./lml_message_protocol");


window.FeatureExtractorText = LMLAlgorithms.FeatureExtractorText;
window.FeatureExtractorNumbers = LMLAlgorithms.FeatureExtractorNumbers;
window.FeatureExtractorImage = LMLAlgorithms.FeatureExtractorImage;
window.FeatureExtractor = LMLAlgorithms.FeatureExtractor;
window.MlAlgorithmNeuralNetwork = LMLAlgorithms.MlAlgorithmNeuralNetwork;
window.MlAlgorithmKNN = LMLAlgorithms.MlAlgorithmKNN;
window.MlAlgorithm = LMLAlgorithms.MlAlgorithm;

window.LabeledDataManagerService = LMLLabeledDataManager.LabeledDataManagerService;

window.lmlRequest = LMLMessageProtocol.lmlRequest;