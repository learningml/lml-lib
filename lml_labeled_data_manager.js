function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function LabeledDataManagerService() {

    this.channel = {
        request: new BroadcastChannel('lwd-request'),
        response: new BroadcastChannel('lwd-response')
    }

    this.saveData = false;

    this.mlEditor = true;
    this.labelsWithData = new Map(); // Map<Label, ExampleData[]>;
    this.modelType = null;
    this.mlAlgorithm = null;
    this.labels = []; // Label[];
    this.name = 'sin nombre';
    this.state = LabeledDataManagerService.State.EMPTY; // UNTRAINED | TRAINED | OUTDATED | EMPTY | TRAINING;
    this.advanced = false;
    this.featureDimension = 2;
    this.featureDimensionLocked = false;
    this.algorithms_params = {
        validationSplit: 0,
        knn: {
            k: 5
        },
        neural_network: {
            epochs: 20,
            batchSize: 10,
            learningRate: 0.001
        }
    };

    this.initMessageBroker();

    let keys_that_change_model_state = [
        "labeledData.labelsWithData",
        "labeledData.labels",
        "labeledData.algorithms_params",
        "labeledData.modelType"
    ];

    window.addEventListener('storage', (evt) => {
        if (keys_that_change_model_state.includes(evt.key)) {
            this.changeState("data_modified");
        }
        this.loadFromLocalStorage();
    })
}

LabeledDataManagerService.State = {
    EMPTY: 0,
    UNTRAINED: 1,
    TRAINING: 2,
    READY: 3,
    OUTDATED: 4
}

LabeledDataManagerService.StateName = [
    "EMPTY",
    "UNTRAINED",
    "TRAINING",
    "READY",
    "OUTDATED"
]

LabeledDataManagerService.prototype.initMessageBroker = function () {
    this.channel.request.addEventListener('message', message => {

        switch (message.data.operation) {

            case 'add_entry_to_label':
                this.addEntry({ data: message.data.args[0], label: message.data.args[1] }, false);
                break;
            case 'remove_entry_from_label':
                this.removeEntry({ data: message.data.args[0], label: message.data.args[1] }, false)
                break;
            case 'add_label':
                this.addLabel(message.data.args[0], false);
                break;
            case 'remove_label':
                this.removeLabel(message.data.args[0], false);
                break;
            case 'reload':
                this.reload();
                break;
            // case 'get_lwd':
            //     let lwdSerialized = this.serializeLabelsWithData();
            //     this.channel.response.postMessage(lwdSerialized);
            //     break;
            // case 'get_labels':
            //     this.channel.response.postMessage(JSON.stringify(this.labels));
            //     break;
        }
    })
}

LabeledDataManagerService.prototype.getStateName = function () {
    return LabeledDataManagerService.StateName[this.state]
}

LabeledDataManagerService.prototype.serializeLabelsWithData = function () {
    if (this.modelType != 'image') {
        return JSON.stringify(Array.from(this.labelsWithData))
    } else {
        let lwd = new Map();
        for (const k of this.labelsWithData.keys()) {
            let srcs = [];
            for (const imageSrc of this.labelsWithData.get(k)) {
                srcs.push(imageSrc);
            }
            lwd.set(k, srcs);
        }
        return JSON.stringify(Array.from(lwd));
    }
}

LabeledDataManagerService.prototype.unserializeLabelsWithData = function (lwdSerialized) {
    let lwd = new Map(JSON.parse(lwdSerialized));
    if (this.modelType != 'image') {
        return lwd;
    } else {
        let lwdImages = new Map();
        for (const k of lwd.keys()) {
            let srcs = [];
            for (const src of lwd.get(k)) {
                srcs.push(src);
            }
            lwdImages.set(k, srcs);
        }
        return lwdImages;
    }
}

LabeledDataManagerService.prototype.loadFromLocalStorage = function () {
    if (localStorage.getItem("labeledData.mlEditor")) {
        this.mlEditor = parseInt(localStorage.getItem("labeledData.mlEditor")) == 1;
    }
    if (localStorage.getItem("labeledData.modelType") != null) {
        this.modelType = localStorage.getItem("labeledData.modelType");
    }
    if (localStorage.getItem("labeledData.mlAlgorithm") != null) {
        this.mlAlgorithm = localStorage.getItem("labeledData.mlAlgorithm");
    }
    if (localStorage.getItem("labeledData.name") != null) {
        this.name = localStorage.getItem("labeledData.name");
    }
    if (localStorage.getItem("labeledData.advanced") != null) {
        this.advanced = parseInt(localStorage.getItem("labeledData.advanced")) == 1
    }
    if (localStorage.getItem("labeledData.featureDimension") != null) {
        this.featureDimension = parseInt(localStorage.getItem("labeledData.featureDimension"));
    }
    if (localStorage.getItem("labeledData.featureDimensionLocked") != null) {
        this.featureDimensionLocked = parseInt(localStorage.getItem("labeledData.featureDimensionLocked")) == 1;
    }
    if (localStorage.getItem("labeledData.algorithms_params") != null) {
        this.algorithms_params = JSON.parse(localStorage.getItem("labeledData.algorithms_params"));
    }
}

LabeledDataManagerService.prototype.saveInLocalStorage = function () {
    localStorage.setItem("labeledData.mlEditor", (this.mlEditor ? 1 : 0).toString());
    localStorage.setItem("labeledData.modelType", this.modelType);
    localStorage.setItem("labeledData.mlAlgorithm", this.mlAlgorithm);
    localStorage.setItem("labeledData.labels", JSON.stringify(this.labels));
    localStorage.setItem("labeledData.name", this.name);
    localStorage.setItem("labeledData.advanced", (this.advanced ? 1 : 0).toString());
    localStorage.setItem("labeledData.featureDimension", this.featureDimension.toString());
    localStorage.setItem("labeledData.featureDimensionLocked", (this.featureDimensionLocked ? 1 : 0).toString());
    localStorage.setItem("labeledData.algorithms_params", JSON.stringify(this.algorithms_params));
}

LabeledDataManagerService.prototype.loadLabelsWithData = function () {
    let message_id = uuidv4();
    this.channel.request.postMessage({
        operation: 'get_lwd',
        message_id: message_id
    });

    let that = this;
    this.channel.response.addEventListener('message', ev => {
        if (ev.data.message_id == message_id) {
            that.labelsWithData = that.unserializeLabelsWithData(ev.data.result);
        }
    })
}

LabeledDataManagerService.prototype.loadLabels = function () {
    let message_id = uuidv4();
    this.channel.request.postMessage({
        operation: 'get_labels',
        message_id: message_id
    });

    let that = this;
    this.channel.response.addEventListener('message', ev => {
        if (ev.data.message_id == message_id) {
            that.labels = JSON.parse(ev.data.result);
        }
    })
}
LabeledDataManagerService.prototype.setMLEditor = function (mlEditor) {
    this.mlEditor = mlEditor;
    localStorage.setItem("labeledData.mlEditor", (this.mlEditor ? 1 : 0).toString());
}

LabeledDataManagerService.prototype.setModelType = function (modelType) {
    if (modelType != this.modelType) this.clearData();
    this.modelType = modelType;
    localStorage.setItem("labeledData.modelType", modelType);
}

LabeledDataManagerService.prototype.setMLAlgorithm = function (algorithm) {
    this.mlAlgorithm = algorithm;
    localStorage.setItem("labeledData.mlAlgorithm", this.mlAlgorithm);
}

LabeledDataManagerService.prototype.setName = function (name) {
    this.name = name;
    localStorage.setItem("labeledData.name", this.name);
}

LabeledDataManagerService.prototype.setState = function (state) {
    this.state = state;
}

LabeledDataManagerService.prototype.setAdvanced = function (isAdvanced) {
    this.advanced = isAdvanced;
    localStorage.setItem("labeledData.advanced", (this.advanced ? 1 : 0).toString());
}

LabeledDataManagerService.prototype.setFeatureDimension = function (dimension) {
    this.featureDimension = dimension;
    localStorage.setItem("labeledData.featureDimension", this.featureDimension.toString());
}

LabeledDataManagerService.prototype.setFeatureDimensionLocked = function (isLocked) {
    this.featureDimensionLocked = isLocked;
    localStorage.setItem("labeledData.featureDimensionLocked", (this.featureDimensionLocked ? 1 : 0).toString());
}

LabeledDataManagerService.prototype.setAlgorithmsParams = function (param, value) {
    switch (param) {
        case 'knn.k':
            this.algorithms_params.knn.k = value;
            break;
        case 'neural_network.epochs':
            this.algorithms_params.neural_network.epochs = value;
            break;
        case 'neural_network.batchSize':
            this.algorithms_params.neural_network.batchSize = value;
            break;
        case 'neural_network.learningRate':
            this.algorithms_params.neural_network.learningRate = value;
            break;
        case 'validationSplit':
            this.algorithms_params.validationSplit = value;
            break;
    }
    localStorage.setItem("labeledData.algorithms_params", JSON.stringify(this.algorithms_params));
}

LabeledDataManagerService.prototype.getTotalNumberOfExamples = function () {
    let n = 0;
    for (let label of this.labels) {
        n += this.labelsWithData.get(label).length
    }
    return n;
}

LabeledDataManagerService.prototype.clear = function () {
    console.log("CLEARRRRRRRR");
    this.labelsWithData.clear();
    this.modelType = null;
    this.mlAlgorithm = null;
    this.labels = [];
    this.setName('sin nombre');
    this.setState(LabeledDataManagerService.State.EMPTY);
    this.setAdvanced(false);
    this.setFeatureDimension(2);
    this.setFeatureDimensionLocked(false);
    this.setAlgorithmsParams("validationSplit", 0);
    this.setAlgorithmsParams("knn.k", 5);
    this.setAlgorithmsParams("neural_network.epochs", 20);
    this.setAlgorithmsParams("neural_network.batchSize", 10);
    this.setAlgorithmsParams("neural_network.learningRate", 0.001);

    this.saveInLocalStorage();
}

LabeledDataManagerService.prototype.clearData = function () {
    console.log("clearData");
    this.labelsWithData.clear();
    this.labels = [];
    this.setState(LabeledDataManagerService.State.EMPTY);
}

LabeledDataManagerService.prototype.get = function (label) {
    return this.labelsWithData.get(label);
}

LabeledDataManagerService.prototype.save = function () {
    const blob = new Blob([this.serializeModel()], { type: 'application/json' });
    saveAs(blob, this.name);
}

LabeledDataManagerService.prototype.changeState = function (action) {
    if (action == 'data_modified') {
        if (this.state == LabeledDataManagerService.State.EMPTY) this.setState(LabeledDataManagerService.State.UNTRAINED);
        if (this.state == LabeledDataManagerService.State.UNTRAINED) this.setState(LabeledDataManagerService.State.UNTRAINED);
        if (this.state == LabeledDataManagerService.State.READY) this.setState(LabeledDataManagerService.State.OUTDATED);
        if (this.state == LabeledDataManagerService.State.OUTDATED) this.setState(LabeledDataManagerService.State.OUTDATED);
    } else if (action == 'train') {
        this.setState(LabeledDataManagerService.State.TRAINING);
    } else if (action == 'end') {
        this.setState(LabeledDataManagerService.State.READY);
    }
}

LabeledDataManagerService.prototype.addLabel = function (label, notify = true) {
    // Add label only if not exits
    if (label == "") return;
    if (!this.labelsWithData.has(label)) {
        //this.labels.push(label);
        // esto es para detectar cambios en el array
        this.labels = [label].concat(this.labels);
        this.labelsWithData.set(label, []);
        this.changeState('data_modified');
        if (notify) {
            this.channel.request.postMessage({
                operation: 'add_label',
                args: [label]
            })
        }
    }
}

LabeledDataManagerService.prototype.removeLabel = function (label, notify = true) {
    // Esto es una manera INMUTABLE de borrar un elemento de un array
    // Necesito que sea una operación inmutable para que angular se 
    // dé cuenta de que el array ha cambiado.
    this.labels = this.labels.filter(o => o != label);

    this.labelsWithData.delete(label);
    if (this.labels.length == 0) {
        this.setState(LabeledDataManagerService.State.EMPTY);
    }
    this.changeState('data_modified');
    if (notify) {
        this.channel.request.postMessage({
            operation: 'remove_label',
            args: [label]
        })
    }
}

LabeledDataManagerService.prototype.addEntry = function (entry, notify = true) {
    console.log("ADD ENTRY kuku");
    let label = entry.label;
    let data = entry.data;
    if (label == "" || data == "" ||
        label == null || data == null ||
        label == undefined || data == undefined) return;
    this.addLabel(label, false);
    if (this.labelsWithData.get(label).indexOf(data) == -1) {
        this.labelsWithData.set(label, this.labelsWithData.get(label).concat(data));
        this.changeState('data_modified');
        if (notify) {
            this.channel.request.postMessage({
                operation: 'add_entry_to_label',
                args: [data, label]
            });
        }

    }
}

LabeledDataManagerService.prototype.removeEntry = function (entry, notify = true) {
    let label = entry.label;
    let data = entry.data;

    this.labelsWithData.set(label, this.labelsWithData.get(label).filter(o => o != data))
    this.changeState('data_modified');
    if (notify) {
        this.channel.request.postMessage({
            operation: 'remove_entry_from_label',
            args: [data, label]
        })
    }
}

LabeledDataManagerService.prototype.reload = function () {
    this.loadFromLocalStorage();
    this.loadLabelsWithData();
    this.loadLabels();
    this.setState(LabeledDataManagerService.State.EMPTY);
}


LabeledDataManagerService.prototype.truncateNumbers = function (numbersCSV) {
    let numbers = [];
    let numbersString = numbersCSV.split(",");
    for (let numberString of numbersString) {
        numbers.push(parseFloat(numberString).toFixed(4));
    }

    return numbers.join(",");
}

LabeledDataManagerService.prototype.csvToObject = function (str, delimiter = ";") {

    const rows = str.slice(0).split("\r\n");

    // Map the rows
    // split values from each row into an array
    // use headers.reduce to create an object
    // object properties derived from headers:values
    // the object passed as an element of the array
    let obj = {
        data: {},
        type: "numerical"
    }
    rows.forEach(row => {
        const values = row.split(delimiter);
        let _class = values[0];
        values.shift();
        let str_values = values.toString();
        if (!(_class in obj.data)) {
            obj.data[_class] = [];
        }

        obj.data[_class].push(str_values);
    });

    // return the array
    return obj;
}

LabeledDataManagerService.prototype.load = function (file, file_format) {
    let inputData;

    this.clear();

    try {
        if (file_format == "text/csv") {
            inputData = this.csvToObject(file);
        } else {
            inputData = JSON.parse(file);
        }
        Object.keys(inputData.data).forEach(key => {
            this.modelType = inputData.type;
            let data = inputData.data[key];
            for (let d of data) {
                if (this.modelType == 'text') {
                    this.addEntry({ label: key, data: d });
                }
                if (this.modelType == 'numerical') {
                    let _d = this.truncateNumbers(d);
                    this.addEntry({ label: key, data: _d });
                    this.featureDimension = d.split(",").length;
                    this.featureDimensionLocked = true;
                }
                if (this.modelType == 'image') {
                    let i = new Image();
                    i.src = d;
                    this.addEntry({ label: key, data: i });
                }
            }
        });
    }
    catch {
        alert("Fichero erróneo. No puedo interpretar ese fichero. ¿Seguro que está bien construido?");
    }
}

LabeledDataManagerService.prototype.serializeModel = function () {
    let dataObject = {
        type: this.modelType,
        data: {}
    };
    for (let label of this.labelsWithData.keys()) {
        dataObject.data[label] = [];
        for (let data of this.labelsWithData.get(label)) {
            if (this.modelType == "text" || this.modelType == "numerical") {
                dataObject.data[label].push(data);
            }
            if (this.modelType == "image") {
                dataObject.data[label].push(data['src']);
            }

        }
    }
    let dataJSON = JSON.stringify(dataObject);

    return dataJSON;
}

module.exports = {
    LabeledDataManagerService
}